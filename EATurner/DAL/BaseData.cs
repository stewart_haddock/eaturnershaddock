﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

namespace EATurner.DAL
{
    public class BaseData : IDisposable
    {
        protected SqlConnection connection;
        protected SqlTransaction transaction;
        protected string connectionString;

        public BaseData()
        {
            connectionString = ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString;
            connection = new SqlConnection(connectionString);

        }
        
        public BaseData(string databaseconnectionstring)
        {
            connectionString = databaseconnectionstring;
        }

        public BaseData(SqlConnection databaseConnection)
        {
            connection = databaseConnection;
        }

        public BaseData(SqlConnection databaseConnection, SqlTransaction sqlTransaction)
        {
            connection = databaseConnection;
            transaction = sqlTransaction;
        }

        protected string GetQuery(SqlCommand cmd)
        {
            string query = cmd.CommandText;
            try
            {
                foreach (SqlParameter pa in cmd.Parameters)
                {
                    query += " " + pa.ParameterName + "='" + (pa.Value.ToString() ?? "") + "',";
                }
            }
            catch
            {
                query = cmd.CommandText + ":::There was a bad parameter in the query";
            }

            return query.TrimEnd(',');
        }

        public void Dispose()
        {
            connection.Close();
        }
    }

}
