﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace EATurner.DAL
{
    public class MovieData : BaseData
    {
        public SqlDataReader GetTitlesReader(string title)
        {
            SqlCommand cmd = new SqlCommand();
            try 
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "uspTitleList";
                SqlParameter param = new SqlParameter("@TitleName", title);
                cmd.Parameters.Add(param);
                connection.Open();
                cmd.Connection = connection;
                SqlDataReader reader;
                reader = cmd.ExecuteReader();
                return reader;
            }
            catch 
            {
                throw;
            }
        }

        public SqlDataReader GetTitlesDetailsReader(int titleId)
        {
            SqlCommand cmd = new SqlCommand();
            try 
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "uspTitleGet";
                SqlParameter param = new SqlParameter("@TitleId", titleId.ToString());
                cmd.Parameters.Add(param);
                connection.Open();
                cmd.Connection = connection;
                SqlDataReader reader;
                reader = cmd.ExecuteReader();
                return reader;
            }
            catch 
            {
                throw;
            }
        }
    }

}