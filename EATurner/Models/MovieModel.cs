﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;

namespace EATurner.Models
{
    public class MovieModel
    {
        public int TitleId { get; set; }
        [Required(ErrorMessage="Title Is Required")]
        public string TitleName { get; set; }
        public string TitleNameSortable { get; set; }
        public int TitleTypeId { get; set; }
        public int ReleaseYear { get; set; }
        public DateTime ProcessedDateTimeUTC { get; set; }
        public string Participant { get; set; }
        public string ParticipantType { get; set; }
        public string Description { get; set; }

        public List<MovieModel> GetMovies(string MovieTitle)
        {
            List<MovieModel> models = new List<MovieModel>();
            DAL.MovieData md = new DAL.MovieData();
            SqlDataReader reader = md.GetTitlesReader(MovieTitle);
            while (reader.Read())
            {
                MovieModel model = new MovieModel();
                model.TitleId = int.Parse(reader["TitleId"].ToString());
                model.TitleName = reader["TitleName"].ToString();
                models.Add(model);
                
            }
            return models;
        }

        public void GetMovieDetails(int MovieId)
        {
            DAL.MovieData md = new DAL.MovieData();
            SqlDataReader reader = md.GetTitlesDetailsReader(MovieId);
            while (reader.Read())
            {                               
                TitleId = int.Parse(reader["TitleId"].ToString());
                TitleName = reader["TitleName"].ToString();
                TitleNameSortable = reader["TitleNameSortable"].ToString();
                int ttid = 0;
                int.TryParse(reader["TitleTypeId"].ToString(), out ttid);
                    TitleTypeId = ttid;
                int ry = 0;
                int.TryParse(reader["ReleaseYear"].ToString(),out ry);
                ReleaseYear = ry;
                Description = reader["Description"].ToString();
                break;
            }
        }
    }
}