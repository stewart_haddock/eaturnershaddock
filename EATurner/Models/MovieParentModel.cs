﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EATurner.Models
{
    public class MovieParentModel
    {
        public List<MovieModel> Movies { get; set; }
        public MovieModel Movie { get; set; }
        public bool DisplayDetails { get; set; }
    }
}