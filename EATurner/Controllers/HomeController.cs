﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EATurner.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return RedirectToAction("Index", "Movie");

            // ViewBag.Message = "Welcome to ASP.NET MVC!";
            //return View();
        }

        public ActionResult About()
        {
            return View();
        }
    }
}
