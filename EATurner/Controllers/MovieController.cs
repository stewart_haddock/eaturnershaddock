﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace EATurner.Controllers
{
    public class MovieController : Controller
    {
        //
        // GET: /Book/

        public ActionResult Index()
        {
            ViewBag.Message = "Search For A Movie";
            return View();
        }

        public ActionResult Search(string TitleName)
        {
            Session["searchedTitle"] = TitleName;
            //ViewBag.SearchedTitle = TitleName;
            Models.MovieParentModel movieParent = new Models.MovieParentModel();
            movieParent.Movies = getMovies(TitleName);
            movieParent.DisplayDetails = false;
            ViewBag.Message = "Movie Count: " + movieParent.Movies.Count.ToString();
            return View("Index", movieParent);
        }

        private List<Models.MovieModel> getMovies(string TitleName)
        {
            Models.MovieModel movieModel = new Models.MovieModel();
            List<Models.MovieModel> movies = movieModel.GetMovies(TitleName);
            return movies;
        }

        //
        // GET: /Book/Details/5

        public ActionResult Details(int id)
        {
            Models.MovieParentModel movieParent = new Models.MovieParentModel();
            Models.MovieModel movieModel = new Models.MovieModel();
            movieModel.GetMovieDetails(id);
            movieParent.Movie = movieModel;
            movieParent.Movies = getMovies(Session["searchedTitle"].ToString());
            movieParent.DisplayDetails = true;
            return View("Index", movieParent);

        }

        //
        // GET: /Book/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /Book/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        
        //
        // GET: /Book/Edit/5
 
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Book/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Book/Delete/5
 
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Book/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
